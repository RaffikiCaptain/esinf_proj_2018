package proj;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class SymbolNodeTest {
    private SymbolTree validTree = new SymbolTree();

    private LinkedList<SymbolNode> letterList = new LinkedList<>();
    private SymbolNode ms1 = new SymbolNode(".", "E", SymbolNodeClass.LETTER);
    private SymbolNode ms2 = new SymbolNode("_", "T", SymbolNodeClass.LETTER);
    private SymbolNode ms3 = new SymbolNode("..", "I", SymbolNodeClass.LETTER);
    private SymbolNode ms4 = new SymbolNode("._", "A", SymbolNodeClass.LETTER);
    private SymbolNode ms5 = new SymbolNode("__", "M", SymbolNodeClass.LETTER);
    private SymbolNode ms6 = new SymbolNode("_.", "N", SymbolNodeClass.LETTER);
    private SymbolNode ms7 = new SymbolNode("_..", "D", SymbolNodeClass.LETTER);
    private SymbolNode ms8 = new SymbolNode("__.", "G", SymbolNodeClass.LETTER);
    private SymbolNode ms9 = new SymbolNode("_._", "K", SymbolNodeClass.LETTER);
    private SymbolNode ms10 = new SymbolNode("___", "O", SymbolNodeClass.LETTER);
    private SymbolNode ms11 = new SymbolNode("._.", "R", SymbolNodeClass.LETTER);
    private SymbolNode ms12 = new SymbolNode("...", "S", SymbolNodeClass.LETTER);
    private SymbolNode ms13 = new SymbolNode(".._", "U", SymbolNodeClass.LETTER);
    private SymbolNode ms14 = new SymbolNode(".__", "W", SymbolNodeClass.LETTER);
    private SymbolNode ms15 = new SymbolNode("_...", "B", SymbolNodeClass.LETTER);
    private SymbolNode ms16 = new SymbolNode("_._.", "C", SymbolNodeClass.LETTER);
    private SymbolNode ms17 = new SymbolNode(".._.", "F", SymbolNodeClass.LETTER);
    private SymbolNode ms18 = new SymbolNode("....", "H", SymbolNodeClass.LETTER);
    private SymbolNode ms19 = new SymbolNode(".___", "J", SymbolNodeClass.LETTER);
    private SymbolNode ms20 = new SymbolNode("._..", "L", SymbolNodeClass.LETTER);
    private SymbolNode ms21 = new SymbolNode(".__.", "P", SymbolNodeClass.LETTER);
    private SymbolNode ms22 = new SymbolNode("__._", "Q", SymbolNodeClass.LETTER);
    private SymbolNode ms23 = new SymbolNode("..._", "V", SymbolNodeClass.LETTER);
    private SymbolNode ms24 = new SymbolNode("_.._", "X", SymbolNodeClass.LETTER);
    private SymbolNode ms25 = new SymbolNode("_.__", "Y", SymbolNodeClass.LETTER);
    private SymbolNode ms26 = new SymbolNode("__..", "Z", SymbolNodeClass.LETTER);

    private LinkedList<SymbolNode> numberList = new LinkedList<>();
    private SymbolNode ms31 = new SymbolNode(".____", "1", SymbolNodeClass.NUMBER);
    private SymbolNode ms32 = new SymbolNode("..___", "2", SymbolNodeClass.NUMBER);
    private SymbolNode ms33 = new SymbolNode("...__", "3", SymbolNodeClass.NUMBER);
    private SymbolNode ms34 = new SymbolNode("...._", "4", SymbolNodeClass.NUMBER);
    private SymbolNode ms35 = new SymbolNode(".....", "5", SymbolNodeClass.NUMBER);
    private SymbolNode ms36 = new SymbolNode("_....", "6", SymbolNodeClass.NUMBER);
    private SymbolNode ms37 = new SymbolNode("__...", "7", SymbolNodeClass.NUMBER);
    private SymbolNode ms38 = new SymbolNode("___..", "8", SymbolNodeClass.NUMBER);
    private SymbolNode ms39 = new SymbolNode("____.", "9", SymbolNodeClass.NUMBER);
    private SymbolNode ms40 = new SymbolNode("_____", "0", SymbolNodeClass.NUMBER);

    private LinkedList<SymbolNode> punctuationList = new LinkedList<>();
    private SymbolNode ms41 = new SymbolNode("__.._", "#", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms42 = new SymbolNode("_.._.", "/", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms43 = new SymbolNode("._...", "&", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms44 = new SymbolNode("._._.", "+", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms45 = new SymbolNode("_..._", "=", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms58 = new SymbolNode("_.__..", "(", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms59 = new SymbolNode("_.__._", ")", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms60 = new SymbolNode("___...", ":", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms61 = new SymbolNode("._._._", ".", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms62 = new SymbolNode("__..__", ",", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms63 = new SymbolNode("..__..", "?", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms64 = new SymbolNode(".____.", "'", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms65 = new SymbolNode("_._.__", "!", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms66 = new SymbolNode("_._._.", ";", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms67 = new SymbolNode("_...._", "-", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms68 = new SymbolNode("..__._", "_", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms69 = new SymbolNode("._.._.", "\"", SymbolNodeClass.PUNCTUATION);
    private SymbolNode ms70 = new SymbolNode(".__._.", "@", SymbolNodeClass.PUNCTUATION);

    private LinkedList<SymbolNode> nonEnglishList = new LinkedList<>();
    private SymbolNode ms27 = new SymbolNode("._._", "Ä", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms28 = new SymbolNode("..__", "Ü", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms29 = new SymbolNode("___.", "Ö", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms30 = new SymbolNode("____", "CH", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms46 = new SymbolNode(".__._", "À", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms47 = new SymbolNode("_._..", "Ç", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms48 = new SymbolNode("..__.", "Ð", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms49 = new SymbolNode("._.._", "È", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms50 = new SymbolNode(".._..", "É", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms51 = new SymbolNode("__._.", "Ĝ", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms52 = new SymbolNode("_.__.", "Ĥ", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms53 = new SymbolNode(".___.", "Ĵ", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms54 = new SymbolNode("__.__", "Ñ", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms55 = new SymbolNode("..._.", "Ŝ", SymbolNodeClass.NONENGLISH);
    private SymbolNode ms56 = new SymbolNode(".__..", "Þ", SymbolNodeClass.NONENGLISH);

    private SymbolNode ms57 = new SymbolNode("_._._", "<CT>", SymbolNodeClass.PROSIGN);

    @BeforeEach
    void setUp() {
        validTree.importMorseFile();

        letterList.add(ms1);
        letterList.add(ms2);
        letterList.add(ms3);
        letterList.add(ms4);
        letterList.add(ms5);
        letterList.add(ms6);
        letterList.add(ms7);
        letterList.add(ms8);
        letterList.add(ms9);
        letterList.add(ms10);
        letterList.add(ms11);
        letterList.add(ms12);
        letterList.add(ms13);
        letterList.add(ms14);
        letterList.add(ms15);
        letterList.add(ms16);
        letterList.add(ms17);
        letterList.add(ms18);
        letterList.add(ms19);
        letterList.add(ms20);
        letterList.add(ms21);
        letterList.add(ms22);
        letterList.add(ms23);
        letterList.add(ms24);
        letterList.add(ms25);
        letterList.add(ms26);

        numberList.add(ms31);
        numberList.add(ms32);
        numberList.add(ms33);
        numberList.add(ms34);
        numberList.add(ms35);
        numberList.add(ms36);
        numberList.add(ms37);
        numberList.add(ms38);
        numberList.add(ms39);
        numberList.add(ms40);

        punctuationList.add(ms41);
        punctuationList.add(ms42);
        punctuationList.add(ms43);
        punctuationList.add(ms44);
        punctuationList.add(ms45);
        punctuationList.add(ms58);
        punctuationList.add(ms59);
        punctuationList.add(ms60);
        punctuationList.add(ms61);
        punctuationList.add(ms62);
        punctuationList.add(ms63);
        punctuationList.add(ms64);
        punctuationList.add(ms65);
        punctuationList.add(ms66);
        punctuationList.add(ms67);
        punctuationList.add(ms68);
        punctuationList.add(ms69);
        punctuationList.add(ms70);

        nonEnglishList.add(ms27);
        nonEnglishList.add(ms28);
        nonEnglishList.add(ms29);
        nonEnglishList.add(ms30);
        nonEnglishList.add(ms46);
        nonEnglishList.add(ms47);
        nonEnglishList.add(ms48);
        nonEnglishList.add(ms49);
        nonEnglishList.add(ms50);
        nonEnglishList.add(ms51);
        nonEnglishList.add(ms52);
        nonEnglishList.add(ms53);
        nonEnglishList.add(ms54);
        nonEnglishList.add(ms55);
        nonEnglishList.add(ms56);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testImportMorseFile() {
        // contains all predicted members in the tree
        assertEquals(6, validTree.height());
        assertEquals(71, validTree.size());
        assertTrue(validTree.containsAll(letterList));
        assertTrue(validTree.containsAll(numberList));
        assertTrue(validTree.containsAll(punctuationList));
        assertTrue(validTree.containsAll(nonEnglishList));
        assertTrue(validTree.contains(ms57));

        assertFalse(validTree.contains(new SymbolNode(".......", "", SymbolNodeClass.ROOT)));

        assertEquals(0, ms35.compareTo(validTree.smallestElement()));
        validTree.remove(ms35);
        assertFalse(validTree.containsAll(numberList));
        assertFalse(validTree.contains(ms35));

        assertEquals(0, ms18.compareTo(validTree.smallestElement()));
        validTree.remove(ms18);
        assertFalse(validTree.contains(ms18));

        assertEquals(0, ms34.compareTo(validTree.smallestElement()));
        validTree.remove(ms34);
        assertFalse(validTree.contains(ms34));

        assertEquals(6, validTree.height());
        assertEquals(68, validTree.size());

        validTree.remove(ms41);
        validTree.remove(ms42);
        validTree.remove(ms43);
        validTree.remove(ms44);
        validTree.remove(ms45);
        validTree.remove(ms58);
        validTree.remove(ms59);
        validTree.remove(ms60);
        validTree.remove(ms61);
        validTree.remove(ms62);
        validTree.remove(ms63);
        validTree.remove(ms64);
        validTree.remove(ms65);
        validTree.remove(ms66);
        validTree.remove(ms67);
        validTree.remove(ms68);
        validTree.remove(ms69);
        validTree.remove(ms70);

        assertEquals(5, validTree.height());
        assertEquals(50, validTree.size());
    }

    @Test
    void testDecodeMessage() {
        // multi character example
        String validCode = ". . .. ._ __ _____ .___";
        String validText = "EEIAM0J";
        assertEquals(validText, validTree.decodeMessage(validCode));

        // encoding characters not existent in the encoding tree
        String validCode2 = ".__._ Á Ã";
        String validText2 = "ÀÁÃ";
        assertEquals(validText2, validTree.decodeMessage(validCode2));

        // requested test
        String validCode3 = ". ... .. _. .._.";
        String validText3 = "ESINF";
        assertEquals(validText3, validTree.decodeMessage(validCode3));

        // replacing . and _ and spaces
        String validCode4 = "._._._ ..__._ __.. / ..__._ ._._._";
        String validText4 = "._Z _.";
        assertEquals(validText4, validTree.decodeMessage(validCode4));
    }

    @Test
    void testCreateClassTree() {
        ValueTree letterTree = validTree.createEncodingTree(SymbolNodeClass.LETTER);

        // in a letter tree there are all letter characters
        assertTrue(letterTree.containsAll(letterList));

        // unless a letter is removed
        letterTree.remove(new ValueNode(ms1.getSymbol(), ms1.getValue()/*, ms1.getSymbolClass()*/));
        assertFalse(letterTree.containsAll(letterList));

        // in a letter tree there are neither numbers, punctuations and non english characters
        for (SymbolNode tmpSymbolNode : numberList) {
            assertFalse(letterTree.contains(tmpSymbolNode));
        }
        for (SymbolNode tmpSymbolNode : punctuationList) {
            assertFalse(letterTree.contains(tmpSymbolNode));
        }
        for (SymbolNode tmpSymbolNode : nonEnglishList) {
            assertFalse(letterTree.contains(tmpSymbolNode));
        }
    }

    @Test
    void testEncodeMessage() {
        // multicharachter example
        String validText1 = "EEIAM0J";
        String validCode1 = ". . .. ._ __ _____ .___";
        assertEquals(validCode1, validTree.encodeMessage(validText1, SymbolNodeClass.ROOT));

        // encoding characters not existent in the encoding tree
        String validText2 = "ÀÁÃ";
        String validCode2 = ".__._ Á Ã";
        assertEquals(validCode2, validTree.encodeMessage(validText2, SymbolNodeClass.ROOT));

        // requested test of specific tree letters
        String validText3 = "ESINF";
        String validCode3 = ". ... .. _. .._.";
        assertEquals(validCode3, validTree.encodeMessage(validText3, SymbolNodeClass.LETTER));

        // replacing . and _ and spaces
        String validText4 = "._Z _.";
        String validCode4 = "._._._ ..__._ __.. / ..__._ ._._._";
        assertEquals(validCode4, validTree.encodeMessage(validText4, SymbolNodeClass.ROOT));
    }

    @Test
    void a5_commonSequenceTest() {
        // simple comparison
        assertEquals(".", validTree.commonSequence(ms1.getSymbol(), ms3.getSymbol()));
        assertNotEquals("_", validTree.commonSequence(ms1.getSymbol(), ms3.getSymbol()));
        assertEquals("_", validTree.commonSequence(ms2.getSymbol(), ms5.getSymbol()));
        assertNotEquals(".", validTree.commonSequence(ms2.getSymbol(), ms5.getSymbol()));

        // no match
        assertEquals("", validTree.commonSequence(ms1.getSymbol(), ms2.getSymbol()));

        // example given in question back and forth
        assertEquals("....", validTree.commonSequence(ms34.getSymbol(), ms35.getSymbol()));
        assertEquals("....", validTree.commonSequence(ms35.getSymbol(), ms34.getSymbol()));

        // match between two equal symbols is the symbol itself
        assertEquals(ms35.getSymbol(), validTree.commonSequence(ms35.getSymbol(), ms35.getSymbol()));
    }

    @Test
    void a6_orderedOccurrenceListByClassTest() {
        // provided list
        List<String> list = new LinkedList<>();
        list.add("!2C");
        list.add("A23");
        list.add("AB3");
        list.add("!'=");

        Map<SymbolNodeClass, List<String>> expected = new LinkedHashMap<>();

        // list of letters in order
        List<String> listLetter = new LinkedList<>();
        listLetter.add("AB3");
        listLetter.add("!2C");
        listLetter.add("A23");
        expected.put(SymbolNodeClass.LETTER, listLetter);
        // list of numbers in order
        List<String> listNumber = new LinkedList<>();
        listNumber.add("A23");
        listNumber.add("!2C");
        listNumber.add("AB3");
        expected.put(SymbolNodeClass.NUMBER, listNumber);
        // list of punctuators in order
        List<String> listPunctuation = new LinkedList<>();
        listPunctuation.add("!'=");
        listPunctuation.add("!2C");
        expected.put(SymbolNodeClass.PUNCTUATION, listPunctuation);

        // BST implementation
        assertEquals(expected, validTree.orderedOccurrenceListByClassBST(list));
    }

}