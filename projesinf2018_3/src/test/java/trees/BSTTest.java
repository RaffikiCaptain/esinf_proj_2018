package trees;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * @author DEI-ESINF
 */
public class BSTTest {
    static Integer[] arr = {20, 15, 10, 13, 8, 17, 40, 50, 30, 7};
    static int[] height = {0, 1, 2, 3, 3, 3, 3, 3, 3, 4};
    static Integer[] inOrderT = {7, 8, 10, 13, 15, 17, 20, 30, 40, 50};
    static Integer[] preOrderT = {20, 15, 10, 8, 7, 13, 17, 40, 30, 50};
    static Integer[] posOrderT = {7, 8, 13, 10, 17, 15, 30, 50, 40, 20};

    public BSTTest() {
    }

    public BST<Integer> setUp() {
        BST<Integer> instance = new BST<>();
        for (int i : arr)
            instance.insert(i);
        return instance;
    }

    /**
     * Test of size method, of class BST.
     */
    @Test
    public void testSize() {
        BST<Integer> instance = setUp();
        Assertions.assertEquals(arr.length, instance.size());

        BST<String> sInstance = new BST<>();
        Assertions.assertEquals(0, sInstance.size());
        sInstance.insert("A");
        Assertions.assertEquals(1, sInstance.size());
        sInstance.insert("B");
        Assertions.assertEquals(2, sInstance.size());
        sInstance.insert("A");
        Assertions.assertEquals(2, sInstance.size());
    }

    /**
     * Test of insert method, of class BST.
     */
    @Test
    public void testInsert() {
        int[] arr = {20, 15, 10, 13, 8, 17, 40, 50, 30, 20, 15, 10};
        BST<Integer> instance = new BST<>();
        for (int i = 0; i < 9; i++) {            //new elements
            instance.insert(arr[i]);
            Assertions.assertEquals(instance.size(), i + 1);
        }
        for (int i = 9; i < arr.length; i++) {    //duplicated elements => same size
            instance.insert(arr[i]);
            Assertions.assertEquals(9, instance.size());
        }
    }

    /**
     * Test of remove method, of class BST.
     */
    @Test
    public void testRemove() {
        BST<Integer> instance = setUp();
        int qtd = arr.length;
        instance.remove(999);

        Assertions.assertEquals(instance.size(), qtd);
        for (int i = 0; i < arr.length; i++) {
            instance.remove(arr[i]);
            qtd--;
            Assertions.assertEquals(qtd, instance.size());
        }

        instance.remove(999);
        Assertions.assertEquals(0, instance.size());
    }

    /**
     * Test of isEmpty method, of class BST.
     */
    @Test
    public void testIsEmpty() {
        BST<Integer> instance = setUp();
        Assertions.assertFalse(instance.isEmpty());
        instance = new BST<>();
        Assertions.assertTrue(instance.isEmpty());

        instance.insert(11);
        Assertions.assertFalse(instance.isEmpty());

        instance.remove(11);
        Assertions.assertTrue(instance.isEmpty());
    }

    /**
     * Test of height method, of class BST.
     */
    @Test
    public void testHeight() {
        BST<Integer> instance;
        instance = new BST<>();
        Assertions.assertEquals(instance.height(), -1);
        for (int idx = 0; idx < arr.length; idx++) {
            instance.insert(arr[idx]);
            Assertions.assertEquals(instance.height(), height[idx]);
        }
        instance = new BST<>();
        Assertions.assertEquals(instance.height(), -1);
    }

    /**
     * Test of smallestelement method, of class TREE.
     */
    @Test
    public void testSmallestElement() {
        BST<Integer> instance = setUp();
        Assertions.assertEquals(new Integer(7), instance.smallestElement());
        instance.remove(7);
        Assertions.assertEquals(new Integer(8), instance.smallestElement());
        instance.remove(8);
        Assertions.assertEquals(new Integer(10), instance.smallestElement());
    }

    /**
     * Test of processBstByLevel method, of class TREE.
     */
    @Test
    public void testProcessBstByLevel() {
        BST<Integer> instance = setUp();
        Map<Integer, List<Integer>> expResult = new HashMap<>();
        expResult.put(0, Collections.singletonList(20));
        expResult.put(1, Arrays.asList(15, 40));
        expResult.put(2, Arrays.asList(10, 17, 30, 50));
        expResult.put(3, Arrays.asList(8, 13));
        expResult.put(4, Collections.singletonList(7));

        Map<Integer, List<Integer>> result = instance.nodesByLevel();

        for (Map.Entry<Integer, List<Integer>> e : result.entrySet())
            Assertions.assertEquals(expResult.get(e.getKey()), e.getValue());
    }


    /**
     * Test of inOrder method, of class BST.
     */
    @Test
    public void testInOrder() {
        BST<Integer> instance = setUp();
        List<Integer> lExpected = Arrays.asList(inOrderT);
        Assertions.assertEquals(lExpected, instance.inOrder());
    }

    /**
     * Test of preOrder method, of class BST.
     */
    @Test
    public void testpreOrder() {
        BST<Integer> instance = setUp();
        List<Integer> lExpected = Arrays.asList(preOrderT);
        Assertions.assertEquals(lExpected, instance.preOrder());
    }

    /**
     * Test of posOrder method, of class BST.
     */
    @Test
    public void testposOrder() {
        BST<Integer> instance = setUp();
        List<Integer> lExpected = Arrays.asList(posOrderT);
        Assertions.assertEquals(lExpected, instance.posOrder());
    }
}
