package proj;

import trees.BST;

import java.util.List;

public class ValueTree extends BST<ValueNode> {

    /**
     * method to verify the existence of one symbol in a tree
     * used for tests
     *
     * @param symbolNode
     * @return
     */
    public boolean contains(SymbolNode symbolNode) {
        return this.find(new ValueNode(symbolNode.getSymbol(), symbolNode.getValue())) != null;
    }

    /**
     * method to verify the existence of all the symbols of a list in a tree
     * used for tests
     *
     * @param symbolNodeList
     * @return
     */
    public boolean containsAll(List<SymbolNode> symbolNodeList) {
        for (SymbolNode tmpSymbolNode : symbolNodeList) {
            if (this.find(new ValueNode(tmpSymbolNode.getSymbol(), tmpSymbolNode.getValue())) == null) {
                return false;
            }
        }
        return true;
    }

    public String encodeMessage(String decodedMessage) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < decodedMessage.length(); i++) {
            Node<ValueNode> tmpCharNode = this.find(new ValueNode("", Character.toString(decodedMessage.charAt(i))));
            if (tmpCharNode != null) {
                result.append(tmpCharNode.getElement().getSymbol()).append(" ");
            } else {
                result.append(decodedMessage.charAt(i)).append(" ");
            }
        }

        return result.toString().trim();
    }
}
