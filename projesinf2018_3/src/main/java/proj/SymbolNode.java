package proj;

import java.util.Objects;

/**
 * class used for decoding a symbol into a value
 */
public class SymbolNode implements Comparable<SymbolNode> {

    private final String symbol;
    private final String value;
    private final SymbolNodeClass symbolNodeClass;

    public SymbolNode(String symbol, String value, SymbolNodeClass symbolNodeClass) {
        this.symbol = symbol;
        this.value = value;
        this.symbolNodeClass = symbolNodeClass;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getValue() {
        return value;
    }

    public SymbolNodeClass getSymbolNodeClass() {
        return symbolNodeClass;
    }

    @Override
    public int compareTo(SymbolNode that) {

        // get minimum String length to compare first characters
        int compSize = Math.min(this.symbol.length(), that.symbol.length());

        // compare first characters
        for (int i = 0; i < compSize; i++) {
            if ((int) this.symbol.charAt(i) < (int) that.symbol.charAt(i)) {
                return -1;
            } else if ((int) this.symbol.charAt(i) > (int) that.symbol.charAt(i)) {
                return 1;
            }
        }

        // which string should be compared next
        int thisOrThat = Integer.compare(this.symbol.length(), that.symbol.length());

        if (thisOrThat > 0) {
            return ((int) this.symbol.charAt(compSize)) - 47;
        } else if (thisOrThat < 0) {
            return -1 * (((int) that.symbol.charAt(compSize)) - 47);
        }

        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SymbolNode that = (SymbolNode) o;
        return symbol.equals(that.symbol) &&
                value.equals(that.value) &&
                symbolNodeClass == that.symbolNodeClass;
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, value, symbolNodeClass);
    }
}