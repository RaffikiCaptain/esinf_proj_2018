package proj;

import java.util.Objects;

/**
 * class used for encoding a value into a symbol
 */
public class ValueNode implements Comparable<ValueNode> {

    private final String symbol;
    private final String value;

    public ValueNode(String symbol, String value) {
        this.symbol = symbol;
        this.value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public int compareTo(ValueNode that) {
        return value.compareTo(that.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValueNode that = (ValueNode) o;
        return symbol.equals(that.symbol) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, value);
    }
}
