package proj;

import java.util.Objects;

/**
 * class used to store words in order according to a symbolClass
 */
public class WordClassCount implements Comparable<WordClassCount> {
    private final String word;
    private final int symbolClassQuantity;

    public WordClassCount(String word, int symbolClassQuantity) {
        this.word = word;
        this.symbolClassQuantity = symbolClassQuantity;
    }

    public String getWord() {
        return word;
    }

    @Override
    public int compareTo(WordClassCount o) {
        if (o.symbolClassQuantity == symbolClassQuantity)
            return 1;
        return o.symbolClassQuantity - symbolClassQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordClassCount that = (WordClassCount) o;
        return symbolClassQuantity == that.symbolClassQuantity &&
                word.equals(that.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, symbolClassQuantity);
    }
}