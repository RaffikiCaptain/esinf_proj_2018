package proj;

import trees.BST;
import utils.CSVImporter;

import java.util.*;

public class SymbolTree extends BST<SymbolNode> {

    /**
     * Morse Tree constructor
     * Automatically adds empty node at root
     */
    public SymbolTree() {
        super();
        insert(new SymbolNode("/", " ", SymbolNodeClass.ROOT));
    }

    /**
     * method to verify the existence of one symbol in a tree
     * used for tests
     *
     * @param symbolNode
     * @return
     */
    public boolean contains(SymbolNode symbolNode) {
        return this.find(symbolNode) != null;
    }

    /**
     * method to verify the existence of all the symbols of a list in a tree
     * used for tests
     *
     * @param symbolNodeList
     * @return
     */
    public boolean containsAll(List<SymbolNode> symbolNodeList) {
        for (SymbolNode tmpSymbolNode : symbolNodeList) {
            if (this.find(tmpSymbolNode) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Construction of Morse Code
     * Values from the morse_v3.csv
     */
    public void importMorseFile() {
        List<String> morseLines = CSVImporter.readMorse("morse_v3");

        for (String line : morseLines) {
            try {
                String[] lineValues = line.split(" ");
                SymbolNodeClass sClass = SymbolNodeClass.valueOf(lineValues[2].replace("-", "").toUpperCase());
                insert(new SymbolNode(lineValues[0], lineValues[1], sClass));
            } catch (Exception e) {
                System.out.println("line not printed -> " + line);
                e.printStackTrace();
            }
        }
    }

    /**
     * method used to decode a sequence of symbols in a single string into an intelligible text
     *
     * @param codedMessage
     * @return
     */
    public String decodeMessage(String codedMessage) {
        StringBuilder result = new StringBuilder();

        String[] msgCodes = codedMessage.split(" ");

        for (String msgCode : msgCodes) {
            Node<SymbolNode> tmpSymbolNode = this.find(new SymbolNode(msgCode, "", SymbolNodeClass.ROOT));
            if (tmpSymbolNode != null) {
                result.append(tmpSymbolNode.getElement().getValue());
            } else {
                result.append(msgCode);
            }
        }

        return result.toString();
    }

    /**
     * public method to create an encoding tree according to the provided class of symbols
     *
     * @param symbolNodeClass
     * @return
     */
    public ValueTree createEncodingTree(SymbolNodeClass symbolNodeClass) {
        ValueTree result = new ValueTree();

        boolean allFlag = symbolNodeClass == SymbolNodeClass.ROOT;

        createEncodingTree(result, root, symbolNodeClass, allFlag);

        return result;
    }

    /**
     * private recursive method to create sub tree according to the provided class of symbols
     *
     * @param result
     * @param node
     * @param symbolNodeClass
     */
    private void createEncodingTree(ValueTree result, Node<SymbolNode> node, SymbolNodeClass symbolNodeClass, boolean allFlag) {
        if (node != null) {
            if (node.getElement().getSymbolNodeClass() == symbolNodeClass || allFlag) {
                SymbolNode nodeElem = node.getElement();
                result.insert(new ValueNode(nodeElem.getSymbol(), nodeElem.getValue()));
            }
            createEncodingTree(result, node.getLeft(), symbolNodeClass, allFlag);
            createEncodingTree(result, node.getRight(), symbolNodeClass, allFlag);
        }
    }

    /**
     * method used to encode an intelligible text into a sequence of symbols in a single string into
     *
     * @param decodedMessage
     * @param treeType
     * @return
     */
    public String encodeMessage(String decodedMessage, SymbolNodeClass treeType) {
        ValueTree encodeTree = createEncodingTree(treeType);

        return encodeTree.encodeMessage(decodedMessage.toUpperCase());
    }

    /**
     * method which compares character by character of different symbols
     * until one is different returning all previously maatching characters
     *
     * @param firstSymbol
     * @param secondSymbol
     * @return
     */
    public String commonSequence(String firstSymbol, String secondSymbol) {
        StringBuilder result = new StringBuilder();

        int compSize = Math.min(firstSymbol.length(), secondSymbol.length());

        for (int i = 0; i < compSize; i++) {
            if ((int) firstSymbol.charAt(i) == (int) secondSymbol.charAt(i)) {
                result.append(firstSymbol.charAt(i));
            } else {
                return result.toString();
            }
        }

        return result.toString();
    }

    /**
     * Using the properties of a BST data structure, this method returns a map of sequences of words
     * each map object key matches a symbol class
     * and each sequence of word is in decreasing order for the symbol class of the matching object key
     *
     * @param wordList
     * @return
     */
    public Map<SymbolNodeClass, List<String>> orderedOccurrenceListByClassBST(List<String> wordList) {
        Map<SymbolNodeClass, List<String>> result = new LinkedHashMap<>();
        Map<SymbolNodeClass, BST<WordClassCount>> wordTrees = new TreeMap<>();

        for (String tmpText : wordList) {
            String tmpCode = this.encodeMessage(tmpText, SymbolNodeClass.ROOT);

            Map<SymbolNodeClass, Integer> tmpMap = new TreeMap<>();

            String[] chars = tmpCode.split(" ");
            for (String aChar : chars) {
                SymbolNode tmpSymbolNode = this.find(new SymbolNode(aChar, "", SymbolNodeClass.ROOT)).getElement();
                Integer qty = tmpMap.computeIfAbsent(tmpSymbolNode.getSymbolNodeClass(), k -> 0);
                qty++;
                tmpMap.put(tmpSymbolNode.getSymbolNodeClass(), qty);
            }

            for (SymbolNodeClass tmpMapClass : tmpMap.keySet()) {
                BST<WordClassCount> tmpBST = wordTrees.get(tmpMapClass);
                if (tmpBST == null) {
                    tmpBST = new BST<>();
                    wordTrees.put(tmpMapClass, tmpBST);
                }
                tmpBST.insert(new WordClassCount(tmpText, tmpMap.get(tmpMapClass)));
            }
        }

        for (SymbolNodeClass tmpMapClass : wordTrees.keySet()) {
            List<String> classList = new LinkedList<>();
            for (WordClassCount tmpWord : wordTrees.get(tmpMapClass).inOrder()) {
                classList.add(tmpWord.getWord());
            }
            result.put(tmpMapClass, classList);
        }

        return result;
    }


}