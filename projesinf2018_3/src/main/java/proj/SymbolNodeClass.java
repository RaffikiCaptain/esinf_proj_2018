package proj;

public enum SymbolNodeClass {
    LETTER,
    NUMBER,
    PUNCTUATION,
    NONENGLISH, // the hyfen needs to be removed for the imported vaslue to be a match
    PROSIGN,
    ROOT // enum value given to root and examples where the class is negligible
}