package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class CSVImporter {

    private CSVImporter() {
    }

    public static List<String> readMorse(String file) {
        List<String> result = null;

        String line;

        try (BufferedReader br = new BufferedReader(new FileReader(".//src//main//resources//" + file + ".csv"))) {
            result = new LinkedList<>();

            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}